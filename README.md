# Urna Eletrônica para as eleições de 2018 do DF

Este Projeto contém um programa feito em **C++** capaz de simular o funcionamento de uma urna eletrônica brasileira com dados das eleições de 2018 do Distrito Federal.

Para inicializar o programa siga os comandos abaixo:

## Executar o programa

### Instale o copilador (g++)

###### Debian || Ubuntu || Mint
> `sudo apt get g++`

###### macOS
> `g++`

###### Fedora
> `sudo dnf install gcc-c++`

### Limpe o log da camera

> `make clean`

### Compile o programa

> `make all`

### Execute o programa

> `make run`

## Uso do software

### Configure o máximo de eleitores

![alt](imag/max_eleitores.png)

### Registre os dados do eleitor que irá votar

![alt](imag/cpf.png)
![alt](imag/titulo-de-eleitor.png)
![alt](imag/data_nascimento.png)
![alt](imag/nome.png)

### Digite o número seu voto de acordo com o cargo

![alt](imag/dep-federal.png)

### Após cada voto, confirme sua escolha

![alt](imag/confirmar-dados-voto.png)

### Caso não tenha mais eleitores para votar

![alt](imag/continuar-eleição.png)

### Após a votação é apresentado o log da urna

![alt](imag/log-da-urna.png)

### Digite qualquer botão para receber o resultado da eleição

![alt](imag/resultado.png)