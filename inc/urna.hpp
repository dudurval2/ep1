#ifndef URNA_H
#define URNA_H

#include "candidatos.hpp"
#include "eleitor.hpp"

#include <vector>
#include <string>
#include <fstream> /* using ofstream, ifstream, fstream */
#include <iostream>
#include <iomanip> /* using setw, setfill  */


class Urna
{
    private:

        /* Qntd de eleitores que já votaram */
        int contagem_de_eleitores;

        /* Qntd máxima de possíveis eleitores */
        int max_eleitores;

        /* Vector de Objetos Candidatos para dep. federais */
        std::vector <Candidatos> vec_deputados_federais;

        /* Vector de Objetos Candidatos para dep. distritais */
        std::vector <Candidatos> vec_deputados_distritais;

        /* Vector de Objetos Candidatos para senadores */
        std::vector <Candidatos> vec_senadores;

        /* Vector de Objetos Candidatos para governadores */
        std::vector <Candidatos> vec_governadores;

        /* Vector de Objetos Candidatos para presidentes */
        std::vector <Candidatos> vec_presidentes;

        /* Bool para verificar se a votação ainda está em andamento */
        bool votacao;

    public:
        Urna();
        ~Urna();

        /* Método que inicializar o funcionamento da urna */
        bool init();

        /* Método da urna que interage com a Classe CSV */
        bool CSV_to_urna();

        /* Método para inicializar a urna, com o máximo de eleitores na urna e contagem igual a zero */
        void set_max_eleitores();

        /* Loop de votação da urna */
        bool working();

        /* Método usado para escrever os dados do eleitor no log da urna */
        bool log_eleitor(Eleitor * eleitor);

        bool recebe_voto();

        Candidatos* procura_candidato(int numero_voto, int pos_loop);

        /* Método que irá retornar os resultados da eleição de cada cargo disputado */
        void resultado_das_eleicoes();

        /* Método para acrescentar 1 eleitor na contagem de eleitores da urna */
        void add_contagem_de_eleitores();

        /* Método usado para finalizar a votação, antes do total de eleitores possíveis seja atingido, por exemplo, a urna pode receber até 10 eleitores, mas somente 7 foram votar, assim a eleição acaba sem ter atingido o máximo de eleitores possíveis */
        bool stop_votacao();


        /* Método para adicionar um objeto candidato no vector referente ao cargo disputado */
        void adc_candidato(Candidatos cand);

        /* Método para retornar um candidato de acordo com o número do voto e ao cargo do candidato.
            *   aux_loop é uma variável que indica qual o cargo      do candidato procurado
            
            *   dica_cargo é usado para o caso dos senadores, em     que o número do senador é o mesmo do suplente 1      e do suplente 2, então preciso de uma dica de        qual suplente está sendo procurado
        */
        Candidatos * confere_numero(int numero_votado, int aux_loop, std::string dica_cargo = "notVice");

        /* Método usado para escrever no log da urna o cargo e o número do candidato votado */
        bool log_voto(int numero_votado, std::string cargo);

        /* Método usado para ler o log da urna e escrever no terminal */
        void print_log();

        void imprimi_deputado_votado(Candidatos * candidato);
};

#endif